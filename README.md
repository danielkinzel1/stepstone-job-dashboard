## StepStone Job Notification Visualizer

### Description

Imagine registering to StepStone because you are looking for a job. You search for a specific job position and select you want to receive the information about new added offers as mail.

Like me, you will end up with about 5-15 mails each day including a new job offer. Soon you will get fed up with opening mails and skimming over the text to find out what the job is about, where it is, what it is. All while you have to remember if you actually read this email before or thinking about if you might have forgotten to look into one of the previous mails.

So I decided to create a StreamLit Dashboard that collects and aggregates these mails and offers some filtering options.

### General Idea and concept
- Query mails using [Gmail API](https://developers.google.com/gmail/api) with designated Tag (for example '_stepstone')
- Extract Job Title, Location and company name
- Use [StreamLit](https://docs.streamlit.io/) to visualize data
- Provide several filtering Options (configurable in config.yaml)

### Example
![example_dashboard](docs/dashboard_example.JPG)

![example_dashboard_part_2](docs/dashboard_example_2.JPG)

### Project Structure
TODO

### Features in development
- More Mail Client possibilities (Web, yahoo, gmx, ...)