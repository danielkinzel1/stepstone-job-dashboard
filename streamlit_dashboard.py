from __future__ import print_function

import json

from core.google_client_helper import create_client
from googleapiclient.errors import HttpError
from typing import Dict
from datetime import datetime, timedelta

import yaml
import streamlit as st
import datetime
import pandas as pd
from base64 import urlsafe_b64decode

import altair as alt
import plotly.express as px
import plotly.graph_objs as go

# Config
st.set_page_config(layout="wide")
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
with open("config/config.yaml", "r") as infile:
    config = yaml.load(infile, Loader=yaml.FullLoader)


def process_jobtext(text: str) -> Dict:
    text = text.replace("\r", "")
    field_list = text.split("\n")

    # parse rows of message, as it was seen during implementation
    # is there a more sophisticated way to do this?
    # assert field_list[0] == 'Job Agent Express'
    company_name = field_list[6]
    job_title = field_list[8]

    # Process location
    _location_line = field_list[10].split(":")
    locations = _location_line[1].split("|")[0]

    # Process Salary estimation made by stepstone
    _vals = field_list[12].split("/")[0].replace("€", "").split("-")
    lower_est = int(_vals[0].replace(".", ""))
    upper_est = int(_vals[1].replace(".", ""))

    return {"company": company_name,
            "job": job_title,
            "locations": locations,
            "lowerbound_salary": lower_est,
            "upperbound_salary": upper_est}


@st.cache
def query_jobs(start_date: str, end_date: str) -> pd.DataFrame:
    """
    Write this docstring one day

    :param start_date: the first day with relevant mails
    :param end_date: the last day with relevant mails
    :return: dataframe with mails and extracted content
    """
    try:
        service = create_client(token_path=config['gmail_api_access_token_path'],
                                credentials_path=config['gmail_api_credential_path'])

        # Call the Gmail API for message list
        query = f"in: label:_stepstone after:{start_date} before:{end_date}"
        results = service.users().messages().list(userId="me",
                                                  q=query,
                                                  maxResults=config["gmail_api_max_result_size"]).execute()
        message_ids = [r["id"] for r in results["messages"]]
        dates, companies, jobs, locations, lowerbounds_salary, upperbounds_salary = [], [], [], [], [], []

        # Build batch query
        batch = service.new_batch_http_request()

        for id in message_ids:
            batch.add(service.users().messages().get(userId="me",
                                                     id=id,
                                                     format='full'))
        batch.execute()
        for reponse_key, response in batch._responses.items():
            _response = json.loads(response[1])
            for part in _response['payload']['parts']:
                mimetype = part.get("mimeType")
                body = part.get("body")
                data = body.get("data")
                if mimetype == "text/plain":
                    try:
                        _text_dict = process_jobtext(text=urlsafe_b64decode(data).decode())
                        companies.append(_text_dict["company"])
                        jobs.append(_text_dict["job"])
                        locations.append(_text_dict["locations"])
                        lowerbounds_salary.append(_text_dict["lowerbound_salary"])
                        upperbounds_salary.append(_text_dict["upperbound_salary"])

                        dates.append(str(datetime.datetime.fromtimestamp(int(_response['internalDate']) / 1000)))
                    except Exception as e:
                        print(f"Received non readable Mail with id {id}")

        df = pd.DataFrame({"Datum": dates,
                           "Unternehmen": companies,
                           "Jobtitel": jobs,
                           "Ort(e)": locations,
                           "Gehaltschätzung - von": lowerbounds_salary,
                           "Gehaltschätzung - bis": upperbounds_salary})

        return df

    except HttpError as error:
        print(f'An error occurred: {error}')


# Set up Default Data
df = pd.DataFrame({"Datum": [],
                   "Job": []})

# Site Definition
st.header("Dashboard for Stepstone Job Notifications")

with st.sidebar:
    # TODO add state management (or other concept) to prevent re-query of data when selecting filters after query
    yesterday = datetime.datetime.now() - timedelta(1)
    begin = st.date_input(label="Begin Suchzeitraum", key="begin", value=yesterday.date())
    end = st.date_input(label="Ende Suchzeitraum", key="end", value=datetime.datetime.now().date())

    location_filters = dict()
    for location in config["location_filters"]:
        location_filters[location] = st.checkbox(label=f"In {location}", key=f"includes_{location}")

    st.write(f"Es werden insgesamt maximal {config['gmail_api_max_result_size']} Ergebnisse angezeigt")

if not end < begin:
    df = query_jobs(start_date=begin.strftime("%Y/%#m/%#d"),
                    end_date=(end + timedelta(1)).strftime("%Y/%#m/%#d"))

    chosen_locations = "|".join([key for key, value in location_filters.items() if value])
    if chosen_locations:
        df = df[df["Ort(e)"].str.contains(chosen_locations)]
else:
    df = pd.DataFrame()
    st.write("Anfangsdatum der Suche darf nicht vor dem Enddatum der Suche liegen!")

if st.button("Lade Job Infos"):
    st.experimental_rerun()

st.write(df)

# Prepare Location Statistics
df_mutated = df.copy(deep=True)
locations = df_mutated["Ort(e)"].values
locations_lists = [loc_list.split(",") for loc_list in locations]
location_list = [item for sublist in locations_lists for item in sublist]
locations_unique = list(set(location_list))
locations_count = [location_list.count(loc) for loc in locations_unique]
df_location_counts = pd.DataFrame({"Ort": locations_unique,
                                   "Anzahl": locations_count})
df_location_counts.sort_values(by=["Anzahl"], ascending=False, inplace=True)

chart_loc_count = alt.Chart(df_location_counts).mark_bar().encode(
    x=alt.X("Ort", sort=None), y="Anzahl").configure_axisX(grid=False, labelAngle=45)
chart_loc_count.configure_axisY(tickMinStep=1.0)
st.subheader("Häufigkeit Einsatzorte bei angezeigten Job Offers")
st.altair_chart(chart_loc_count, use_container_width=True)

# Prepare Salary Estimation Statistics
st.subheader("Gehaltsband Schätzungen")
df_mutated["avg Gehalt"] = (df_mutated["Gehaltschätzung - von"] + df_mutated["Gehaltschätzung - bis"]) / 2
df_mutated.sort_values(by="avg Gehalt", inplace=True, ascending=False)

fig1 = px.scatter(y=df_mutated["Gehaltschätzung - von"].values)
fig1.update_traces(marker=dict(size=10, symbol="diamond", color="orange"))
fig2 = px.scatter(y=df_mutated["Gehaltschätzung - bis"].values)
fig2.update_traces(marker=dict(size=10, symbol="diamond", color="orange"))
fig3 = px.line(y=df_mutated["avg Gehalt"].values)
fig4 = go.Figure(data=fig1.data + fig2.data + fig3.data)
fig4.update_layout(yaxis_title="Gehaltsspanne mit Mittelwert", xaxis_title="Jobs absteigend sortiert",
                   xaxis=dict(tickmode="array", tickvals=[x for x in range(df_mutated.shape[0])], ticktext=df_mutated["Jobtitel"].values))

st.plotly_chart(fig4, use_container_width=True)
